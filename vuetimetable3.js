//components
Vue.component('allTeachers',{
	props: ['periodList','dayList','teacherList'],
	template: 	`
					<div>
						<teacher-resourceTable v-for='teacher in teacherList' :teacher=teacher :periodList=periodList :dayList=dayList></teacher-resourceTable>
					</div>
				` 
})

Vue.component('teacher-resourceTable',{
	props: ['periodList','dayList','teacher'],
	template: 	`
					<div class='timetable'>
						<div class='resourceName' v-on:click="show = !show">{{teacher.name}}</div>
						<div class='tableContent' v-show = 'show'>
							<div class='placeholder'></div>
							<div class='dayList'>
								<div class='day' v-for='day in dayList'>{{day}}</div>
							</div>
							<div class='periodList'>
								<div class='period' v-for='period in periodList'>{{period}}</div>
							</div>
							<div class='timeslots'>
								<teacher-resource-timeslot v-for='tslot in teacher.availTable' :tslot=tslot :teacher=teacher></teacher-resource-timeslot>
							</div>
						</div>
					</div>
				`,
	data:	function(){
		return{
			show: false
		}
	}
})

Vue.component('allRooms',{
	props: ['periodList','dayList','roomList'],
	template: 	`
					<div>
						<room-resourceTable v-for='room in roomList' :room=room :periodList=periodList :dayList=dayList></room-resourceTable>
					</div>
				` 
})

Vue.component ('room-resourceTable',{
	props: ['periodList','dayList','room'],
	template: 	`
					<div class='timetable'>
						<div class='resourceName' v-on:click="show = !show">{{room.name}}</div>
						<div class='tableContent' v-show = 'show'>
							<div class='placeholder'></div>
							<div class='dayList'>
								<div class='day' v-for='day in dayList'>{{day}}</div>
							</div>
							<div class='periodList'>
								<div class='period' v-for='period in periodList'>{{period}}</div>
							</div>
							<div class='timeslots'>
								<room-resource-timeslot v-for='tslot in room.availTable' :tslot=tslot :room=room></room-resource-timeslot>
							</div>
						</div>
					</div>
				`,
	data:	function(){
		return{
			show: false
		}
	}
})

Vue.component('allClasses',{
	props: ['periodList','dayList','classList','teacherList','roomList'],
	template: 	`
					<div>
						<class-requestTable v-for='clas in classList' :clas=clas :periodList=periodList :dayList=dayList :teacherList=teacherList :roomList = roomList></class-requestTable>
					</div>
				` 
})

Vue.component('class-requestTable',{
	props: ['periodList','dayList','clas','teacherList','roomList'],
	template: 	`
					<div class='timetable'>
						<div class='resourceName' v-on:click="show = !show">{{clas.name}}</div>
						<div class='tableContent' v-show = 'show'>
							<div class='placeholder'></div>
							<div class='dayList'>
								<div class='day' v-for='day in dayList'>{{day}}</div>
							</div>
							<div class='periodList'>
								<div class='period' v-for='period in periodList'>{{period}}</div>
							</div>
							<div class='timeslots'>
								<class-request-timeslot v-for='tslot in clas.requestTable' :tslot=tslot :clas=clas></class-request-timeslot>
							</div>
							<possibleTeachersList :teacherList=teacherList :clas=clas></possibleTeachersList>
							<possibleRoomsList :roomList = roomList :clas=clas></possibleRoomsList>
						</div>
					</div>
				`,
	data:	function(){
		return{
			show: false
		}
	}			
})

Vue.component('possibleTeachersList',{
	props: ['teacherList','clas'],
	template: 	`
					<div class='possibleTeachersList'>
						<possibleTeacher v-for='teacher in teacherList' :clas=clas :teacher=teacher></possibleTeacher>
					</div>
				`
})

Vue.component('possibleTeacher',{
	props: ['teacher','clas'],
	template: 	`
					<div class='possibleTeacher' v-bind:class='{"selected": selected, "unselected": !selected}' v-on:click='selected = !selected,queue(clas.teachRequestList)' keep-alive>{{teacher.name}}</div>
				`,
	data: 	function(){
		return{
			selected: false
		}
	},
	methods: {
		queue: function(requestList){
			//console.log(this.selected);
			if (this.selected == true){
				requestList.push(this.teacher);
			}else{
				var index = requestList.indexOf(this.teacher); // O(n)
				requestList.splice(index,1);
			}
		}
	}  
})

Vue.component('possibleRoomsList',{
	props: ['roomList','clas'],
	template: 	`
					<div class=possibleRoomsList>
						<possibleRoom v-for='room in roomList' :clas=clas :room=room></possibleRoom>
					</div>
				`,
})

Vue.component('possibleRoom',{
	props: ['room','clas'],
	template: 	`
					<div class='possibleRoom' v-bind:class='{"selected": selected, "unselected": !selected}' v-on:click='selected = !selected,queue(clas.roomRequestList)'>{{room.name}}</div>
				`,
	data: 	function(){
		return{
			selected: false
		}
	},
	methods: {
		queue: function(requestList){
			//console.log(this.selected);
			if (this.selected == true){
				requestList.push(this.room);
			}else{
				var index = requestList.indexOf(this.room); // O(n)
				requestList.splice(index,1);
			}
		}
	}  
})

Vue.component('teacher-resource-timeslot',{
	props: ['tslot','teacher'],
	template: 	`
					<div class='timeslot' v-bind:class='{"available": tslot.isAvail, "unavailable": !tslot.isAvail}' v-on:click="tslot.isAvail = !tslot.isAvail">
					{{tslot.id}}
					</div>
				`,
	data: 	function(){
		return{

		}
	},
	created: function(){
		this.tslot.id = this.teacher.availTable.indexOf(this.tslot)
	}   
})

Vue.component('room-resource-timeslot',{
	props: ['tslot','room'],
	template: 	`
					<div class='timeslot' v-bind:class='{"available": tslot.isAvail, "unavailable": !tslot.isAvail}' v-on:click="tslot.isAvail = !tslot.isAvail">
					{{tslot.id}}
					</div>
				`,
	data: 	function(){
		return{

		}
	},
	mounted: function(){
		this.tslot.id = this.room.availTable.indexOf(this.tslot)
	}   
})

Vue.component('class-request-timeslot',{
	props: ['tslot','clas'],
	template: 	`
					<div class='timeslot' v-bind:class='{"available": tslot.canStart, "unavailable": !tslot.canStart}' v-on:click="validCheck(clas.span, tslot.row), addRemoveRequested(clas.requestedStartSlots)">
					{{tslot.valid}}
					</div>
				`,//if (validCheck(clas.span, tslot.row)) {tslot.canStart = !tslot.canStart}
	data: 	function(){
		return{

		}
	},
	methods: {
		validCheck: function(span,row){
			if ((row-1) + span > 13){ //13 represents the number of periods in the timetable
				console.log("Invalid Timeslot");
			}else{
				this.tslot.canStart = !this.tslot.canStart;
			}
		},
		addRemoveRequested: function(startSlots){
			if (this.tslot.canStart == true){
				startSlots.push(this.tslot.id);
			}else{
				if (startSlots.length != 0){
					var index = this.clas.requestedStartSlots.indexOf(this.tslot.id); //returns -1 if not in array
					if (index > -1){ //invalid timeslot selection will cause an error without this
						startSlots.splice(index,1);
					}
				}else{
					console.log("Empty List");
				}
			}
			//console.log(startSlots)
		}
	},
	mounted: function(){
		this.tslot.id = this.clas.requestTable.indexOf(this.tslot)
	} 
})

//The Algorithm
Vue.component('algorithm',{
	props: ['classList','teacherList','roomList'],
	template: 	`
					<div>
						<unassignedClasses :unassignedClasses=unassignedClasses :classList=classList></unassignedClasses>
						<tuple :unassignedClasses=unassignedClasses></tuple>
					</div>
				`,
	data: function(){
		return{
			//unassignedClasses: this.classList This binds to the main classList and removes from both
			unassignedClasses: this.copy(),
			targetClass: {}
		}
	},
	methods:{
		copy: function(){
			var copy = [];
			for (var i = 0; i <= (this.classList.length-1); i++){
				//console.log(i);
				var obj = this.classList[i];
				//console.log(obj);
				copy.push(obj);
				//console.log(copy);
			}
			
			return copy;
		},
		setTarget: function(){
			this.targetClass = this.unassignedClasses[0];
		}
	},
	mounted: function(){
		this.setTarget();
	}
})

Vue.component('unassignedClasses',{
	props: ['unassignedClasses','classList'],
	template: 	`
					<div class='unassignedList'>
						<unassignedClass v-for='unassignedClass in unassignedClasses' :unassignedClasses=unassignedClasses :unassignedClass=unassignedClass :classList=classList></unassignedClass>
					</div>
				`,
	methods:{
		sortByDemand: function(){
			this.unassignedClasses.sort(function(a,b){return Math.max(b.roomDemand, b.teacherDemand)-Math.max(a.roomDemand, a.teacherDemand)}) //https://www.w3schools.com/jsref/jsref_sort.asp
		},
	},
	mounted: function(){
		this.sortByDemand();
	}
})

Vue.component('unassignedClass',{
	props: ['unassignedClasses','unassignedClass','classList'],
	template: 	`
					<div class='unassignedClass'>Name: {{unassignedClass.name}}<br>Teacher Demand: {{unassignedClass.teacherDemand}}<br>Room Demand: {{unassignedClass.roomDemand}}<br>Requests: {{unassignedClass.teachRequestList.length}}<br>Available: {{unassignedClass.availTeacherList.length}}</div>
				`,
	data: function(){
		return{

		}
	},
	methods:{
		determineTeacherAvailabilityAndSetRequests: function(){
			this.unassignedClass.availTeach = 0;
			thisClass = this.unassignedClass;
			thisClass.availTeacherList = []; //reset the list
			timeslots = this.unassignedClass.requestedStartSlots;
			index = this.classList.indexOf(this.unassignedClass); // never used as javascript objects are passed by reference
			teacherList = this.unassignedClass.teachRequestList;
			//console.log(teacherList.length)
			if (teacherList.length == 0){
				//console.log("No teachers selected");
			}
			for (var i = 0; i <= (teacherList.length-1); i++){
				teacher = teacherList[i];
				teacherTable = teacher.availTable;
				//console.log('teacher: ',teacher.name);
				if (timeslots.length == 0){
					//console.log("No timeslots requested");
				}else{
					checks = thisClass.span-1;
					for (var a = 0; a <= (timeslots.length-1); a++){
						initialIndex = timeslots[a];
						if (teacherTable[initialIndex].isAvail == true){ //if the class can start at this teacher's timeslot
							nextIndex = initialIndex + 7;
							for (var b = 0; b < checks; checks--){
								//console.log(checks);
								if (teacherTable[nextIndex].isAvail == true){
									nextIndex = nextIndex + 7;
								}else{
									break;
								}
							}
							if (checks == 0){
								this.unassignedClass.availTeach++;
								//console.log('Passed the checks');
								if (teacherTable[initialIndex].requesters.includes(thisClass)){
									//console.log('This class is already requesting this timeslot');
								}else{
									for (var re = 0; re<thisClass.span; re++){
										//console.log(teacher);
										//console.log(teacherTable[initialIndex].requesters);
										teacherTable[initialIndex].requesters.push(thisClass);
										initialIndex = initialIndex + 7;
									}
								}
								if (thisClass.availTeacherList.includes(teacher)){
									//console.log('this is already included')
								}else{
									thisClass.availTeacherList.push(teacher);
								}		
							}else{
								//console.log('failed a check, cannot be assigned');
							}
						}else{
							//console.log('this class cannot start here');
						}
					}
				}
			}
		}, 
		determineRoomAvailabilityAndSetRequests: function(){
			this.unassignedClass.availRooms = 0;
			thisClass = this.unassignedClass;
			thisClass.availRoomList = [];
			timeslots = this.unassignedClass.requestedStartSlots;
			index = this.classList.indexOf(this.unassignedClass);
			roomList = this.unassignedClass.roomRequestList;
			if (roomList.length == 0){
				//console.log("No rooms selected");
			}
			for (var i = 0; i <= (roomList.length-1); i++){
				room = roomList[i];
				roomTable = room.availTable;
				//console.log('room: ', room.name);
				if (timeslots.length == 0){
					//console.log("No timeslots requested");
				}else{
					checks = thisClass.span-1;
					for (var a = 0; a <= (timeslots.length-1); a++){
						initialIndex = timeslots[a];
						if (roomTable[initialIndex].isAvail == true){ //if the class can start at this teacher's timeslot
							nextIndex = initialIndex + 7;
							for (var b = 0; b < checks; checks--){
								//console.log(checks);
								if (roomTable[nextIndex].isAvail == true){
									nextIndex = nextIndex + 7;
								}else{
									break;
								}
							}
							if (checks == 0){
								//console.log('Passed the checks');
								this.unassignedClass.availRooms++;
								if (roomTable[initialIndex].requesters.includes(thisClass)){
									//console.log('This class is already requesting this timeslot');
								}else{
									for (var re = 0; re<thisClass.span; re++){
										//console.log(teacher);
										//console.log(roomTable[initialIndex].requesters);
										roomTable[initialIndex].requesters.push(thisClass);
										initialIndex = initialIndex + 7;
									}
								}
								if (thisClass.availRoomList.includes(room)){
									//console.log('this is already included')
								}else{
									thisClass.availRoomList.push(room);
								}		
							}else{
								//console.log('failed a check, cannot be assigned');
							}
						}else{
							//console.log('this class cannot start here');
						}
					}
				}
			}

		},
		calculateDemands: function(){
			//console.log(this.unassignedClass.teachRequestList);
			if (this.unassignedClass.availTeacherList.length == 0){
				this.unassignedClass.teacherDemand = 0;
				//console.log("Empty " + this.unassignedClass.demand);
			}else{
				this.unassignedClass.teacherDemand = (1/this.unassignedClass.availTeach);
				//console.log("Success " + this.unassignedClass.demand);
			}
			if (this.unassignedClass.availRoomList.length == 0){
				this.unassignedClass.roomDemand = 0;
			}else{
				this.unassignedClass.roomDemand = (1/this.unassignedClass.availRooms);
			}
		}
	},
	mounted: function(){
		console.log("calculating");
		this.determineTeacherAvailabilityAndSetRequests();
		this.determineRoomAvailabilityAndSetRequests();
		this.calculateDemands();
	},
	watch: {
		unassignedClasses: function(){
			console.log("recalculating");
			this.determineTeacherAvailabilityAndSetRequests();
			this.determineRoomAvailabilityAndSetRequests();
			this.calculateDemands();
		}
	}
})


Vue.component('tuple',{
	props: ['unassignedClasses'],
	template: 	`
					<div>
						<div v-if="lesson.teacherDemand > 0 && lesson.roomDemand > 0" class='tuple'>
							<div>{{lesson.name}}</div>
							<div>{{teacher.name}}</div>
							<div>{{room.name}}</div>
							<div>{{tslot}}</div>
						</div>
						<div v-else class='tuple'>
							<div>This cannot continue</div>
							<div v-if="lesson.requestedStartSlots.length <= 0">The lesson, {{lesson.name}}, has no requested timeslots</div>
							<div v-if="lesson.teachRequestList.length <= 0">The lesson, {{lesson.name}}, has no requested teachers</div>
							<div v-if="lesson.teacherDemand <= 0">The lesson, {{lesson.name}}, has no possible teachers</div>
							<div v-if="lesson.roomRequestList.length <= 0">The lesson, {{lesson.name}}, has no requested rooms</div>
							<div v-if="lesson.roomDemand <= 0">The lesson, {{lesson.name}}, has no possible rooms</div>
							
						</div>
						<tupleList :tuple=tuple :unassignedClasses=unassignedClasses></tupleList>
					</div>
				`,
	data: function(){
		return{
			lesson: {},
			teacher: {},
			room: {},
			tslot: {},
			tuple: {}
		}
	},
	methods:{
		determineBestLessonRoomTeacherTuple: function(){
			console.log("started");
			this.lesson = this.unassignedClasses[0];
			thisLesson = this.lesson;
			bestTeacher = null;
			bestTeacherScore = Infinity;
			bestRoom = null;
			bestRoomScore = Infinity;
			bestTupleScore = Infinity;
			possibleTeachers = thisLesson.availTeacherList;
			possibleRooms = thisLesson.availRoomList;
			requestedStartSlots = thisLesson.requestedStartSlots;
			if (possibleTeachers <= 0 || possibleRooms <= 0){
				console.log("no teachers/rooms available");
			}else{
				span = thisLesson.span;
				for (var requestIndex = 0; requestIndex <= (requestedStartSlots.length-1); requestIndex++){
					for (var teacherPos = 0; teacherPos <= (possibleTeachers.length-1); teacherPos++){
						intialTeacherIndex = requestedStartSlots[requestIndex];
						possibleTeacher = possibleTeachers[teacherPos];
						possibleTeacherTable = possibleTeacher.availTable;
						teacherRequestScore = 0;
						if (possibleTeacherTable[intialTeacherIndex].isAvail == true){ //proof of valid timeslot
							nextTeacherIndex = intialTeacherIndex + 7;
							teacherRequestScore = possibleTeacherTable[intialTeacherIndex].requesters.length;
							for (teacherCheck = span-1; teacherCheck > 0; teacherCheck--){
								if (possibleTeacherTable[nextTeacherIndex].isAvail == true){
									nextTeacherIndex = nextTeacherIndex + 7;
									teacherRequestScore = teacherRequestScore + possibleTeacherTable[nextIndex].requesters.length;
								}else{
									break;
								}
							}
							if (teacherCheck == 0){
								for (var roomPos = 0; roomPos <= (possibleRooms.length-1); roomPos++){
									console.log(roomPos);
									intialRoomIndex = requestedStartSlots[requestIndex];
									possibleRoom = possibleRooms[roomPos];
									possibleRoomTable = possibleRoom.availTable;
									roomRequestScore = 0;
									if (possibleRoomTable[intialRoomIndex].isAvail == true){
										nextRoomIndex = intialRoomIndex + 7;
										roomRequestScore = possibleRoomTable[intialRoomIndex].requesters.length;
										for (roomCheck = span-1; roomCheck > 0; roomCheck--){
											if (possibleRoomTable[nextRoomIndex].isAvail == true){
												nextRoomIndex = nextRoomIndex + 7;
												roomRequestScore = roomRequestScore + possibleRoomTable[nextIndex].requesters.length;
											}else{
												break;
											}
										}
										if (roomCheck == 0){
											sumScore = teacherRequestScore + roomRequestScore;
											if (sumScore < bestTupleScore){
												bestStart = requestedStartSlots[requestIndex];
												bestTeacher = possibleTeacher;
												bestRoom = possibleRoom;
												bestTupleScore = sumScore;
											}
										}
									}
								}
							}
						}
					}
				}
				//set parameters for a tuple
				this.teacher = bestTeacher;
				this.room = bestRoom;
				this.tslot= bestStart;
				this.tuple= {	teacher: this.teacher,
								room: this.room,
								slot: this.tslot,
								lesson: thisLesson
							}
				//
				//change timeslot availabilities for assigned resources
				this.teacher.availTable[this.tslot].isAvail = false;
				this.teacher.availTable[this.tslot].assigned = thisLesson;
				this.teacher.availTable[this.tslot].requesters = [];
				this.room.availTable[this.tslot].isAvail = false;
				this.room.availTable[this.tslot].assigned = thisLesson;
				this.room.availTable[this.tslot].requesters = [];
				var nextSlot = this.tslot + 7;
				for (var removeCount = 0; removeCount < (span-1); removeCount++){
					this.teacher.availTable[nextSlot].isAvail = false;
					this.teacher.availTable[nextSlot].assigned = thisLesson;
					this.teacher.availTable[nextSlot].requesters = [];
					this.room.availTable[nextSlot].isAvail = false;
					this.room.availTable[nextSlot].assigned = thisLesson;
					this.room.availTable[nextSlot].requesters = [];
					nextSlot = nextSlot + 7;
				}
				//
				//remove Lesson from all previously available requested resources
				//teachers
				for (var availTeachPos = 0; availTeachPos < thisLesson.availTeacherList.length; availTeachPos++) {
					for (var teachRequestPos = 0; teachRequestPos < thisLesson.requestedStartSlots.length; teachRequestPos++) {
						if (thisLesson.availTeacherList[availTeachPos].availTable[thisLesson.requestedStartSlots[teachRequestPos]].requesters.includes(thisLesson)){
							var removeTeachIndex = thisLesson.availTeacherList[availTeachPos].availTable[thisLesson.requestedStartSlots[teachRequestPos]].requesters.indexOf(thisLesson); // O(n)
							thisLesson.availTeacherList[availTeachPos].availTable[thisLesson.requestedStartSlots[teachRequestPos]].requesters.splice(removeTeachIndex,1);
						}
					}
				}
				//
				//rooms
				for (var availRoomPos = 0; availRoomPos < thisLesson.availTeacherList.length; availRoomPos++) {
					for (var roomRequestPos = 0; roomRequestPos < thisLesson.requestedStartSlots.length; roomRequestPos++) {
						if (thisLesson.availTeacherList[availRoomPos].availTable[thisLesson.requestedStartSlots[roomRequestPos]].requesters.includes(thisLesson)){
							var removeRoomIndex = thisLesson.availTeacherList[availRoomPos].availTable[thisLesson.requestedStartSlots[roomRequestPos]].requesters.indexOf(thisLesson); // O(n)
							thisLesson.availTeacherList[availRoomPos].availTable[thisLesson.requestedStartSlots[roomRequestPos]].requesters.splice(removeRoomIndex,1);
						}
					}
				}
				//
			}		
		}
	},
	updated: function(){
		//this.determineBestLessonRoomTeacherTuple();
	},
	mounted: function(){
		//this.determineBestLessonRoomTeacherTuple();
	},
	created: function(){
		this.determineBestLessonRoomTeacherTuple();
	},
	watch: {
		unassignedClasses: function(){
			//this.lesson = this.unassignedClasses[0];
			this.determineBestLessonRoomTeacherTuple();
		}
	},
})

Vue.component('tupleList',{
	props: ['tuple','unassignedClasses'],
	template: 	`
				<div>
					<div class='tupleList'>
						<div v-for='tuple in tupleList' class='tuple'>{{tuple.lesson.name}}
														</br>{{tuple.room.name}}
														</br>{{tuple.teacher.name}}
														</br>{{tuple.slot}}
														</div>
					</div>
					<OTB :tuple=tuple :tupleList=tupleList :unassignedClasses=unassignedClasses></OTB>
				</div>
				`,
	data: function(){
		return{
			tupleList: []
		}
	},
	methods:{

	}
})

Vue.component('OTB',{
	props: ['tuple','tupleList','unassignedClasses'],
	template:  	`
					<div>
						<button v-on:click='doTheThing()'>The One True Button</button>
					</div>
				`,
	methods:{
		doTheThing: function(){
			this.reduceTheList();
		},
		reduceTheList: function(){
			if (this.unassignedClasses[0].teacherDemand == 0 || this.unassignedClasses[0].roomDemand == 0){
				console.log('cannot continue');
			}else{
				this.unassignedClasses.splice(0,1);
				console.log("Length of array: " + this.unassignedClasses.length);
				console.log("array: " + this.unassignedClasses[0]);
				console.log("array: " + this.unassignedClasses[1]);
				this.sortByDemand();
				this.tupleList.push(this.tuple); //add to completed list
			}
		},
		sortByDemand: function(){
			this.unassignedClasses.sort(function(a,b){return Math.max(b.roomDemand, b.teacherDemand)-Math.max(a.roomDemand, a.teacherDemand)}) //https://www.w3schools.com/jsref/jsref_sort.asp
		},
	}
})

//main vue
new Vue({
	el: '#app',
	template: 	`
				<div class=mainContent>
					<div v-show="!algorithmReady">
						<div class='app-topbar'>
							<div class='barItem' v-for='option in options' v-on:click='selectedOption = option'>{{option}}</div>
						</div>
						<div class='resourceList' v-show='selectedOption=="Class"'>
							<allClasses :classList=classList :periodList=periodList :dayList=dayList :teacherList=teacherList :roomList=roomList></allClasses>
						</div>
						<div class='resourceList' v-show='selectedOption=="Teacher"'>
							<allTeachers :teacherList=teacherList :periodList=periodList :dayList=dayList></allTeachers>
						</div>
						<div class='resourceList' v-show='selectedOption=="Room"'>
							<allRooms :roomList=roomList :periodList=periodList :dayList=dayList></allRooms>
						</div>
					</div>
					<button v-on:click='algorithmReady = !algorithmReady'>Algorithm Ready</button>
					<div id='algorithmWrapper' v-if='algorithmReady'>
						<algorithm :classList=classList :roomList=roomList :teacherList=teacherList></algorithm>
					</div>
				</div>	
				`,
	data: {
		algorithmReady: false,
		options: ['Teacher','Class','Room'],
		selectedOption: 'Class',
		teacherList: [
			{	name: 'Stedman Dennis',
				teacherId: 1,
				requesters: [], //haven't used
				availTable: [
					{id: 1,  row: 1,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 14, row: 1,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 27, row: 1,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 40, row: 1,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 53, row: 1,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 66, row: 1,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 79, row: 1,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 2,  row: 2,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 15, row: 2,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 28, row: 2,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 41, row: 2,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 54, row: 2,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 67, row: 2,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 80, row: 2,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 3,  row: 3,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 16, row: 3,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 29, row: 3,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 42, row: 3,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 55, row: 3,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 68, row: 3,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 81, row: 3,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 4,  row: 4,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 17, row: 4,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 30, row: 4,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 43, row: 4,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 56, row: 4,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 69, row: 4,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 82, row: 4,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 5,  row: 5,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 18, row: 5,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 31, row: 5,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 44, row: 5,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 57, row: 5,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 70, row: 5,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 83, row: 5,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 6,  row: 6,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 19, row: 6,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 32, row: 6,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 45, row: 6,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 58, row: 6,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 71, row: 6,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 84, row: 6,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 7,  row: 7,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 20, row: 7,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 33, row: 7,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 46, row: 7,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 59, row: 7,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 72, row: 7,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 85, row: 7,  col: 7, isAvail: true, assigned: null, requesters: []},
				 	{id: 8,  row: 8,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 21, row: 8,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 34, row: 8,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 47, row: 8,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 60, row: 8,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 73, row: 8,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 86, row: 8,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 9,  row: 9,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 22, row: 9,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 35, row: 9,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 48, row: 9,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 61, row: 9,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 74, row: 9,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 87, row: 9,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 10, row: 10, col: 1, isAvail: true, assigned: null, requesters: []},{id: 23, row: 10, col: 2, isAvail: true, assigned: null, requesters: []},{id: 36, row: 10, col: 3, isAvail: true, assigned: null, requesters: []},{id: 49, row: 10, col: 4, isAvail: true, assigned: null, requesters: []},{id: 62, row: 10, col: 5, isAvail: true, assigned: null, requesters: []},{id: 75, row: 10, col: 6, isAvail: true, assigned: null, requesters: []},{id: 88, row: 10, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 11, row: 11, col: 1, isAvail: true, assigned: null, requesters: []},{id: 24, row: 11, col: 2, isAvail: true, assigned: null, requesters: []},{id: 37, row: 11, col: 3, isAvail: true, assigned: null, requesters: []},{id: 50, row: 11, col: 4, isAvail: true, assigned: null, requesters: []},{id: 63, row: 11, col: 5, isAvail: true, assigned: null, requesters: []},{id: 76, row: 11, col: 6, isAvail: true, assigned: null, requesters: []},{id: 89, row: 11, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 12, row: 12, col: 1, isAvail: true, assigned: null, requesters: []},{id: 25, row: 12, col: 2, isAvail: true, assigned: null, requesters: []},{id: 38, row: 12, col: 3, isAvail: true, assigned: null, requesters: []},{id: 51, row: 12, col: 4, isAvail: true, assigned: null, requesters: []},{id: 64, row: 12, col: 5, isAvail: true, assigned: null, requesters: []},{id: 77, row: 12, col: 6, isAvail: true, assigned: null, requesters: []},{id: 90, row: 12, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 13, row: 13, col: 1, isAvail: true, assigned: null, requesters: []},{id: 26, row: 13, col: 2, isAvail: true, assigned: null, requesters: []},{id: 39, row: 13, col: 3, isAvail: true, assigned: null, requesters: []},{id: 52, row: 13, col: 4, isAvail: true, assigned: null, requesters: []},{id: 65, row: 13, col: 5, isAvail: true, assigned: null, requesters: []},{id: 78, row: 13, col: 6, isAvail: true, assigned: null, requesters: []},{id: 91, row: 13, col: 7, isAvail: true, assigned: null, requesters: []}
				]
			},
			{	name: 'Jonathan Holmes',
				teacherId: 1,
				requesters: [],
				availTable: [
					{id: 1,  row: 1,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 14, row: 1,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 27, row: 1,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 40, row: 1,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 53, row: 1,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 66, row: 1,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 79, row: 1,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 2,  row: 2,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 15, row: 2,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 28, row: 2,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 41, row: 2,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 54, row: 2,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 67, row: 2,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 80, row: 2,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 3,  row: 3,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 16, row: 3,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 29, row: 3,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 42, row: 3,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 55, row: 3,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 68, row: 3,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 81, row: 3,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 4,  row: 4,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 17, row: 4,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 30, row: 4,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 43, row: 4,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 56, row: 4,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 69, row: 4,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 82, row: 4,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 5,  row: 5,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 18, row: 5,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 31, row: 5,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 44, row: 5,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 57, row: 5,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 70, row: 5,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 83, row: 5,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 6,  row: 6,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 19, row: 6,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 32, row: 6,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 45, row: 6,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 58, row: 6,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 71, row: 6,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 84, row: 6,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 7,  row: 7,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 20, row: 7,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 33, row: 7,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 46, row: 7,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 59, row: 7,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 72, row: 7,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 85, row: 7,  col: 7, isAvail: true, assigned: null, requesters: []},
				 	{id: 8,  row: 8,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 21, row: 8,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 34, row: 8,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 47, row: 8,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 60, row: 8,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 73, row: 8,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 86, row: 8,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 9,  row: 9,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 22, row: 9,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 35, row: 9,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 48, row: 9,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 61, row: 9,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 74, row: 9,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 87, row: 9,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 10, row: 10, col: 1, isAvail: true, assigned: null, requesters: []},{id: 23, row: 10, col: 2, isAvail: true, assigned: null, requesters: []},{id: 36, row: 10, col: 3, isAvail: true, assigned: null, requesters: []},{id: 49, row: 10, col: 4, isAvail: true, assigned: null, requesters: []},{id: 62, row: 10, col: 5, isAvail: true, assigned: null, requesters: []},{id: 75, row: 10, col: 6, isAvail: true, assigned: null, requesters: []},{id: 88, row: 10, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 11, row: 11, col: 1, isAvail: true, assigned: null, requesters: []},{id: 24, row: 11, col: 2, isAvail: true, assigned: null, requesters: []},{id: 37, row: 11, col: 3, isAvail: true, assigned: null, requesters: []},{id: 50, row: 11, col: 4, isAvail: true, assigned: null, requesters: []},{id: 63, row: 11, col: 5, isAvail: true, assigned: null, requesters: []},{id: 76, row: 11, col: 6, isAvail: true, assigned: null, requesters: []},{id: 89, row: 11, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 12, row: 12, col: 1, isAvail: true, assigned: null, requesters: []},{id: 25, row: 12, col: 2, isAvail: true, assigned: null, requesters: []},{id: 38, row: 12, col: 3, isAvail: true, assigned: null, requesters: []},{id: 51, row: 12, col: 4, isAvail: true, assigned: null, requesters: []},{id: 64, row: 12, col: 5, isAvail: true, assigned: null, requesters: []},{id: 77, row: 12, col: 6, isAvail: true, assigned: null, requesters: []},{id: 90, row: 12, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 13, row: 13, col: 1, isAvail: true, assigned: null, requesters: []},{id: 26, row: 13, col: 2, isAvail: true, assigned: null, requesters: []},{id: 39, row: 13, col: 3, isAvail: true, assigned: null, requesters: []},{id: 52, row: 13, col: 4, isAvail: true, assigned: null, requesters: []},{id: 65, row: 13, col: 5, isAvail: true, assigned: null, requesters: []},{id: 78, row: 13, col: 6, isAvail: true, assigned: null, requesters: []},{id: 91, row: 13, col: 7, isAvail: true, assigned: null, requesters: []}
				]
			},
			{	name: 'Stephen Wray',
				teacherId: 1,
				requesters: [],
				availTable: [
					{id: 1,  row: 1,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 14, row: 1,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 27, row: 1,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 40, row: 1,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 53, row: 1,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 66, row: 1,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 79, row: 1,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 2,  row: 2,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 15, row: 2,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 28, row: 2,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 41, row: 2,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 54, row: 2,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 67, row: 2,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 80, row: 2,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 3,  row: 3,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 16, row: 3,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 29, row: 3,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 42, row: 3,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 55, row: 3,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 68, row: 3,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 81, row: 3,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 4,  row: 4,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 17, row: 4,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 30, row: 4,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 43, row: 4,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 56, row: 4,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 69, row: 4,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 82, row: 4,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 5,  row: 5,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 18, row: 5,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 31, row: 5,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 44, row: 5,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 57, row: 5,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 70, row: 5,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 83, row: 5,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 6,  row: 6,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 19, row: 6,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 32, row: 6,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 45, row: 6,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 58, row: 6,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 71, row: 6,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 84, row: 6,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 7,  row: 7,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 20, row: 7,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 33, row: 7,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 46, row: 7,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 59, row: 7,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 72, row: 7,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 85, row: 7,  col: 7, isAvail: true, assigned: null, requesters: []},
				 	{id: 8,  row: 8,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 21, row: 8,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 34, row: 8,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 47, row: 8,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 60, row: 8,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 73, row: 8,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 86, row: 8,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 9,  row: 9,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 22, row: 9,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 35, row: 9,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 48, row: 9,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 61, row: 9,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 74, row: 9,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 87, row: 9,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 10, row: 10, col: 1, isAvail: true, assigned: null, requesters: []},{id: 23, row: 10, col: 2, isAvail: true, assigned: null, requesters: []},{id: 36, row: 10, col: 3, isAvail: true, assigned: null, requesters: []},{id: 49, row: 10, col: 4, isAvail: true, assigned: null, requesters: []},{id: 62, row: 10, col: 5, isAvail: true, assigned: null, requesters: []},{id: 75, row: 10, col: 6, isAvail: true, assigned: null, requesters: []},{id: 88, row: 10, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 11, row: 11, col: 1, isAvail: true, assigned: null, requesters: []},{id: 24, row: 11, col: 2, isAvail: true, assigned: null, requesters: []},{id: 37, row: 11, col: 3, isAvail: true, assigned: null, requesters: []},{id: 50, row: 11, col: 4, isAvail: true, assigned: null, requesters: []},{id: 63, row: 11, col: 5, isAvail: true, assigned: null, requesters: []},{id: 76, row: 11, col: 6, isAvail: true, assigned: null, requesters: []},{id: 89, row: 11, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 12, row: 12, col: 1, isAvail: true, assigned: null, requesters: []},{id: 25, row: 12, col: 2, isAvail: true, assigned: null, requesters: []},{id: 38, row: 12, col: 3, isAvail: true, assigned: null, requesters: []},{id: 51, row: 12, col: 4, isAvail: true, assigned: null, requesters: []},{id: 64, row: 12, col: 5, isAvail: true, assigned: null, requesters: []},{id: 77, row: 12, col: 6, isAvail: true, assigned: null, requesters: []},{id: 90, row: 12, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 13, row: 13, col: 1, isAvail: true, assigned: null, requesters: []},{id: 26, row: 13, col: 2, isAvail: true, assigned: null, requesters: []},{id: 39, row: 13, col: 3, isAvail: true, assigned: null, requesters: []},{id: 52, row: 13, col: 4, isAvail: true, assigned: null, requesters: []},{id: 65, row: 13, col: 5, isAvail: true, assigned: null, requesters: []},{id: 78, row: 13, col: 6, isAvail: true, assigned: null, requesters: []},{id: 91, row: 13, col: 7, isAvail: true, assigned: null, requesters: []}
				]
			},
		],
		roomList:[
			{	name: 'LT-10', 
				type: 'lecture', 
				roomId: 1,
				maxCap: 60,
				availTable: [
					{id: 1,  row: 1,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 14, row: 1,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 27, row: 1,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 40, row: 1,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 53, row: 1,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 66, row: 1,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 79, row: 1,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 2,  row: 2,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 15, row: 2,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 28, row: 2,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 41, row: 2,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 54, row: 2,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 67, row: 2,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 80, row: 2,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 3,  row: 3,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 16, row: 3,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 29, row: 3,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 42, row: 3,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 55, row: 3,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 68, row: 3,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 81, row: 3,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 4,  row: 4,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 17, row: 4,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 30, row: 4,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 43, row: 4,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 56, row: 4,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 69, row: 4,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 82, row: 4,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 5,  row: 5,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 18, row: 5,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 31, row: 5,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 44, row: 5,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 57, row: 5,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 70, row: 5,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 83, row: 5,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 6,  row: 6,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 19, row: 6,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 32, row: 6,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 45, row: 6,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 58, row: 6,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 71, row: 6,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 84, row: 6,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 7,  row: 7,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 20, row: 7,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 33, row: 7,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 46, row: 7,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 59, row: 7,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 72, row: 7,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 85, row: 7,  col: 7, isAvail: true, assigned: null, requesters: []},
				 	{id: 8,  row: 8,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 21, row: 8,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 34, row: 8,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 47, row: 8,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 60, row: 8,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 73, row: 8,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 86, row: 8,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 9,  row: 9,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 22, row: 9,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 35, row: 9,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 48, row: 9,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 61, row: 9,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 74, row: 9,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 87, row: 9,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 10, row: 10, col: 1, isAvail: true, assigned: null, requesters: []},{id: 23, row: 10, col: 2, isAvail: true, assigned: null, requesters: []},{id: 36, row: 10, col: 3, isAvail: true, assigned: null, requesters: []},{id: 49, row: 10, col: 4, isAvail: true, assigned: null, requesters: []},{id: 62, row: 10, col: 5, isAvail: true, assigned: null, requesters: []},{id: 75, row: 10, col: 6, isAvail: true, assigned: null, requesters: []},{id: 88, row: 10, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 11, row: 11, col: 1, isAvail: true, assigned: null, requesters: []},{id: 24, row: 11, col: 2, isAvail: true, assigned: null, requesters: []},{id: 37, row: 11, col: 3, isAvail: true, assigned: null, requesters: []},{id: 50, row: 11, col: 4, isAvail: true, assigned: null, requesters: []},{id: 63, row: 11, col: 5, isAvail: true, assigned: null, requesters: []},{id: 76, row: 11, col: 6, isAvail: true, assigned: null, requesters: []},{id: 89, row: 11, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 12, row: 12, col: 1, isAvail: true, assigned: null, requesters: []},{id: 25, row: 12, col: 2, isAvail: true, assigned: null, requesters: []},{id: 38, row: 12, col: 3, isAvail: true, assigned: null, requesters: []},{id: 51, row: 12, col: 4, isAvail: true, assigned: null, requesters: []},{id: 64, row: 12, col: 5, isAvail: true, assigned: null, requesters: []},{id: 77, row: 12, col: 6, isAvail: true, assigned: null, requesters: []},{id: 90, row: 12, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 13, row: 13, col: 1, isAvail: true, assigned: null, requesters: []},{id: 26, row: 13, col: 2, isAvail: true, assigned: null, requesters: []},{id: 39, row: 13, col: 3, isAvail: true, assigned: null, requesters: []},{id: 52, row: 13, col: 4, isAvail: true, assigned: null, requesters: []},{id: 65, row: 13, col: 5, isAvail: true, assigned: null, requesters: []},{id: 78, row: 13, col: 6, isAvail: true, assigned: null, requesters: []},{id: 91, row: 13, col: 7, isAvail: true, assigned: null, requesters: []}
			 	]
			 },
			 {	name: 'LT-9', 
				type: 'lecture', 
				roomId: 1,
				maxCap: 60,
				availTable: [
					{id: 1,  row: 1,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 14, row: 1,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 27, row: 1,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 40, row: 1,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 53, row: 1,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 66, row: 1,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 79, row: 1,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 2,  row: 2,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 15, row: 2,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 28, row: 2,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 41, row: 2,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 54, row: 2,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 67, row: 2,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 80, row: 2,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 3,  row: 3,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 16, row: 3,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 29, row: 3,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 42, row: 3,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 55, row: 3,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 68, row: 3,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 81, row: 3,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 4,  row: 4,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 17, row: 4,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 30, row: 4,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 43, row: 4,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 56, row: 4,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 69, row: 4,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 82, row: 4,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 5,  row: 5,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 18, row: 5,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 31, row: 5,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 44, row: 5,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 57, row: 5,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 70, row: 5,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 83, row: 5,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 6,  row: 6,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 19, row: 6,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 32, row: 6,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 45, row: 6,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 58, row: 6,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 71, row: 6,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 84, row: 6,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 7,  row: 7,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 20, row: 7,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 33, row: 7,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 46, row: 7,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 59, row: 7,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 72, row: 7,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 85, row: 7,  col: 7, isAvail: true, assigned: null, requesters: []},
				 	{id: 8,  row: 8,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 21, row: 8,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 34, row: 8,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 47, row: 8,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 60, row: 8,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 73, row: 8,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 86, row: 8,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 9,  row: 9,  col: 1, isAvail: true, assigned: null, requesters: []},{id: 22, row: 9,  col: 2, isAvail: true, assigned: null, requesters: []},{id: 35, row: 9,  col: 3, isAvail: true, assigned: null, requesters: []},{id: 48, row: 9,  col: 4, isAvail: true, assigned: null, requesters: []},{id: 61, row: 9,  col: 5, isAvail: true, assigned: null, requesters: []},{id: 74, row: 9,  col: 6, isAvail: true, assigned: null, requesters: []},{id: 87, row: 9,  col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 10, row: 10, col: 1, isAvail: true, assigned: null, requesters: []},{id: 23, row: 10, col: 2, isAvail: true, assigned: null, requesters: []},{id: 36, row: 10, col: 3, isAvail: true, assigned: null, requesters: []},{id: 49, row: 10, col: 4, isAvail: true, assigned: null, requesters: []},{id: 62, row: 10, col: 5, isAvail: true, assigned: null, requesters: []},{id: 75, row: 10, col: 6, isAvail: true, assigned: null, requesters: []},{id: 88, row: 10, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 11, row: 11, col: 1, isAvail: true, assigned: null, requesters: []},{id: 24, row: 11, col: 2, isAvail: true, assigned: null, requesters: []},{id: 37, row: 11, col: 3, isAvail: true, assigned: null, requesters: []},{id: 50, row: 11, col: 4, isAvail: true, assigned: null, requesters: []},{id: 63, row: 11, col: 5, isAvail: true, assigned: null, requesters: []},{id: 76, row: 11, col: 6, isAvail: true, assigned: null, requesters: []},{id: 89, row: 11, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 12, row: 12, col: 1, isAvail: true, assigned: null, requesters: []},{id: 25, row: 12, col: 2, isAvail: true, assigned: null, requesters: []},{id: 38, row: 12, col: 3, isAvail: true, assigned: null, requesters: []},{id: 51, row: 12, col: 4, isAvail: true, assigned: null, requesters: []},{id: 64, row: 12, col: 5, isAvail: true, assigned: null, requesters: []},{id: 77, row: 12, col: 6, isAvail: true, assigned: null, requesters: []},{id: 90, row: 12, col: 7, isAvail: true, assigned: null, requesters: []},
			 		{id: 13, row: 13, col: 1, isAvail: true, assigned: null, requesters: []},{id: 26, row: 13, col: 2, isAvail: true, assigned: null, requesters: []},{id: 39, row: 13, col: 3, isAvail: true, assigned: null, requesters: []},{id: 52, row: 13, col: 4, isAvail: true, assigned: null, requesters: []},{id: 65, row: 13, col: 5, isAvail: true, assigned: null, requesters: []},{id: 78, row: 13, col: 6, isAvail: true, assigned: null, requesters: []},{id: 91, row: 13, col: 7, isAvail: true, assigned: null, requesters: []}
			 	]
			 }
		],
		classList:[
			{	name: 'Math', 
				type: 'lecture',
				classId: 1,
				requestedStartSlots: [],
				teachRequestList: [],
				roomRequestList: [],
				availRooms: 0,
				roomDemand: 0,
				availTeach: 0,
				teacherDemand: 0,
				availTeacherList: [],
				availRoomList: [],
				start: null, 
				span: 3,
				requestTable: [
					{id: 1,  row: 1,  col: 1, canStart: false},{id: 14, row: 1,  col: 2, canStart: false},{id: 27, row: 1,  col: 3, canStart: false},{id: 40, row: 1,  col: 4, canStart: false},{id: 53, row: 1,  col: 5, canStart: false},{id: 66, row: 1,  col: 6, canStart: false},{id: 79, row: 1,  col: 7, canStart: false},
			 		{id: 2,  row: 2,  col: 1, canStart: false},{id: 15, row: 2,  col: 2, canStart: false},{id: 28, row: 2,  col: 3, canStart: false},{id: 41, row: 2,  col: 4, canStart: false},{id: 54, row: 2,  col: 5, canStart: false},{id: 67, row: 2,  col: 6, canStart: false},{id: 80, row: 2,  col: 7, canStart: false},
			 		{id: 3,  row: 3,  col: 1, canStart: false},{id: 16, row: 3,  col: 2, canStart: false},{id: 29, row: 3,  col: 3, canStart: false},{id: 42, row: 3,  col: 4, canStart: false},{id: 55, row: 3,  col: 5, canStart: false},{id: 68, row: 3,  col: 6, canStart: false},{id: 81, row: 3,  col: 7, canStart: false},
			 		{id: 4,  row: 4,  col: 1, canStart: false},{id: 17, row: 4,  col: 2, canStart: false},{id: 30, row: 4,  col: 3, canStart: false},{id: 43, row: 4,  col: 4, canStart: false},{id: 56, row: 4,  col: 5, canStart: false},{id: 69, row: 4,  col: 6, canStart: false},{id: 82, row: 4,  col: 7, canStart: false},
			 		{id: 5,  row: 5,  col: 1, canStart: false},{id: 18, row: 5,  col: 2, canStart: false},{id: 31, row: 5,  col: 3, canStart: false},{id: 44, row: 5,  col: 4, canStart: false},{id: 57, row: 5,  col: 5, canStart: false},{id: 70, row: 5,  col: 6, canStart: false},{id: 83, row: 5,  col: 7, canStart: false},
			 		{id: 6,  row: 6,  col: 1, canStart: false},{id: 19, row: 6,  col: 2, canStart: false},{id: 32, row: 6,  col: 3, canStart: false},{id: 45, row: 6,  col: 4, canStart: false},{id: 58, row: 6,  col: 5, canStart: false},{id: 71, row: 6,  col: 6, canStart: false},{id: 84, row: 6,  col: 7, canStart: false},
			 		{id: 7,  row: 7,  col: 1, canStart: false},{id: 20, row: 7,  col: 2, canStart: false},{id: 33, row: 7,  col: 3, canStart: false},{id: 46, row: 7,  col: 4, canStart: false},{id: 59, row: 7,  col: 5, canStart: false},{id: 72, row: 7,  col: 6, canStart: false},{id: 85, row: 7,  col: 7, canStart: false},
			 		{id: 8,  row: 8,  col: 1, canStart: false},{id: 21, row: 8,  col: 2, canStart: false},{id: 34, row: 8,  col: 3, canStart: false},{id: 47, row: 8,  col: 4, canStart: false},{id: 60, row: 8,  col: 5, canStart: false},{id: 73, row: 8,  col: 6, canStart: false},{id: 86, row: 8,  col: 7, canStart: false},
			 		{id: 9,  row: 9,  col: 1, canStart: false},{id: 22, row: 9,  col: 2, canStart: false},{id: 35, row: 9,  col: 3, canStart: false},{id: 48, row: 9,  col: 4, canStart: false},{id: 61, row: 9,  col: 5, canStart: false},{id: 74, row: 9,  col: 6, canStart: false},{id: 87, row: 9,  col: 7, canStart: false},
			 		{id: 10, row: 10, col: 1, canStart: false},{id: 23, row: 10, col: 2, canStart: false},{id: 36, row: 10, col: 3, canStart: false},{id: 49, row: 10, col: 4, canStart: false},{id: 62, row: 10, col: 5, canStart: false},{id: 75, row: 10, col: 6, canStart: false},{id: 88, row: 10, col: 7, canStart: false},
			 		{id: 11, row: 11, col: 1, canStart: false},{id: 24, row: 11, col: 2, canStart: false},{id: 37, row: 11, col: 3, canStart: false},{id: 50, row: 11, col: 4, canStart: false},{id: 63, row: 11, col: 5, canStart: false},{id: 76, row: 11, col: 6, canStart: false},{id: 89, row: 11, col: 7, canStart: false},
			 		{id: 12, row: 12, col: 1, canStart: false},{id: 25, row: 12, col: 2, canStart: false},{id: 38, row: 12, col: 3, canStart: false},{id: 51, row: 12, col: 4, canStart: false},{id: 64, row: 12, col: 5, canStart: false},{id: 77, row: 12, col: 6, canStart: false},{id: 90, row: 12, col: 7, canStart: false},
			 		{id: 13, row: 13, col: 1, canStart: false},{id: 26, row: 13, col: 2, canStart: false},{id: 39, row: 13, col: 3, canStart: false},{id: 52, row: 13, col: 4, canStart: false},{id: 65, row: 13, col: 5, canStart: false},{id: 78, row: 13, col: 6, canStart: false},{id: 91, row: 13, col: 7, canStart: false}
				]
			},
			{	name: 'English', 
				type: 'lecture',
				classId: 2,
				requestedStartSlots: [],
				teachRequestList: [],
				roomRequestList: [],
				availRooms: 0,
				roomDemand: 0,
				availTeach: 0,
				teacherDemand: 0,
				availTeacherList: [],
				availRoomList: [],
				start: null, 
				span: 4,
				requestTable: [
					{id: 1,  row: 1,  col: 1, canStart: false},{id: 14, row: 1,  col: 2, canStart: false},{id: 27, row: 1,  col: 3, canStart: false},{id: 40, row: 1,  col: 4, canStart: false},{id: 53, row: 1,  col: 5, canStart: false},{id: 66, row: 1,  col: 6, canStart: false},{id: 79, row: 1,  col: 7, canStart: false},
			 		{id: 2,  row: 2,  col: 1, canStart: false},{id: 15, row: 2,  col: 2, canStart: false},{id: 28, row: 2,  col: 3, canStart: false},{id: 41, row: 2,  col: 4, canStart: false},{id: 54, row: 2,  col: 5, canStart: false},{id: 67, row: 2,  col: 6, canStart: false},{id: 80, row: 2,  col: 7, canStart: false},
			 		{id: 3,  row: 3,  col: 1, canStart: false},{id: 16, row: 3,  col: 2, canStart: false},{id: 29, row: 3,  col: 3, canStart: false},{id: 42, row: 3,  col: 4, canStart: false},{id: 55, row: 3,  col: 5, canStart: false},{id: 68, row: 3,  col: 6, canStart: false},{id: 81, row: 3,  col: 7, canStart: false},
			 		{id: 4,  row: 4,  col: 1, canStart: false},{id: 17, row: 4,  col: 2, canStart: false},{id: 30, row: 4,  col: 3, canStart: false},{id: 43, row: 4,  col: 4, canStart: false},{id: 56, row: 4,  col: 5, canStart: false},{id: 69, row: 4,  col: 6, canStart: false},{id: 82, row: 4,  col: 7, canStart: false},
			 		{id: 5,  row: 5,  col: 1, canStart: false},{id: 18, row: 5,  col: 2, canStart: false},{id: 31, row: 5,  col: 3, canStart: false},{id: 44, row: 5,  col: 4, canStart: false},{id: 57, row: 5,  col: 5, canStart: false},{id: 70, row: 5,  col: 6, canStart: false},{id: 83, row: 5,  col: 7, canStart: false},
			 		{id: 6,  row: 6,  col: 1, canStart: false},{id: 19, row: 6,  col: 2, canStart: false},{id: 32, row: 6,  col: 3, canStart: false},{id: 45, row: 6,  col: 4, canStart: false},{id: 58, row: 6,  col: 5, canStart: false},{id: 71, row: 6,  col: 6, canStart: false},{id: 84, row: 6,  col: 7, canStart: false},
			 		{id: 7,  row: 7,  col: 1, canStart: false},{id: 20, row: 7,  col: 2, canStart: false},{id: 33, row: 7,  col: 3, canStart: false},{id: 46, row: 7,  col: 4, canStart: false},{id: 59, row: 7,  col: 5, canStart: false},{id: 72, row: 7,  col: 6, canStart: false},{id: 85, row: 7,  col: 7, canStart: false},
			 		{id: 8,  row: 8,  col: 1, canStart: false},{id: 21, row: 8,  col: 2, canStart: false},{id: 34, row: 8,  col: 3, canStart: false},{id: 47, row: 8,  col: 4, canStart: false},{id: 60, row: 8,  col: 5, canStart: false},{id: 73, row: 8,  col: 6, canStart: false},{id: 86, row: 8,  col: 7, canStart: false},
			 		{id: 9,  row: 9,  col: 1, canStart: false},{id: 22, row: 9,  col: 2, canStart: false},{id: 35, row: 9,  col: 3, canStart: false},{id: 48, row: 9,  col: 4, canStart: false},{id: 61, row: 9,  col: 5, canStart: false},{id: 74, row: 9,  col: 6, canStart: false},{id: 87, row: 9,  col: 7, canStart: false},
			 		{id: 10, row: 10, col: 1, canStart: false},{id: 23, row: 10, col: 2, canStart: false},{id: 36, row: 10, col: 3, canStart: false},{id: 49, row: 10, col: 4, canStart: false},{id: 62, row: 10, col: 5, canStart: false},{id: 75, row: 10, col: 6, canStart: false},{id: 88, row: 10, col: 7, canStart: false},
			 		{id: 11, row: 11, col: 1, canStart: false},{id: 24, row: 11, col: 2, canStart: false},{id: 37, row: 11, col: 3, canStart: false},{id: 50, row: 11, col: 4, canStart: false},{id: 63, row: 11, col: 5, canStart: false},{id: 76, row: 11, col: 6, canStart: false},{id: 89, row: 11, col: 7, canStart: false},
			 		{id: 12, row: 12, col: 1, canStart: false},{id: 25, row: 12, col: 2, canStart: false},{id: 38, row: 12, col: 3, canStart: false},{id: 51, row: 12, col: 4, canStart: false},{id: 64, row: 12, col: 5, canStart: false},{id: 77, row: 12, col: 6, canStart: false},{id: 90, row: 12, col: 7, canStart: false},
			 		{id: 13, row: 13, col: 1, canStart: false},{id: 26, row: 13, col: 2, canStart: false},{id: 39, row: 13, col: 3, canStart: false},{id: 52, row: 13, col: 4, canStart: false},{id: 65, row: 13, col: 5, canStart: false},{id: 78, row: 13, col: 6, canStart: false},{id: 91, row: 13, col: 7, canStart: false}
				]
			},
			{	name: 'IT', 
				type: 'lecture',
				classId: 2,
				requestedStartSlots: [],
				teachRequestList: [],
				roomRequestList: [],
				availRooms: 0,
				roomDemand: 0,
				availTeach: 0,
				teacherDemand: 0,
				availTeacherList: [],
				availRoomList: [],
				start: null, 
				span: 1,
				requestTable: [
					{id: 1,  row: 1,  col: 1, canStart: false},{id: 14, row: 1,  col: 2, canStart: false},{id: 27, row: 1,  col: 3, canStart: false},{id: 40, row: 1,  col: 4, canStart: false},{id: 53, row: 1,  col: 5, canStart: false},{id: 66, row: 1,  col: 6, canStart: false},{id: 79, row: 1,  col: 7, canStart: false},
			 		{id: 2,  row: 2,  col: 1, canStart: false},{id: 15, row: 2,  col: 2, canStart: false},{id: 28, row: 2,  col: 3, canStart: false},{id: 41, row: 2,  col: 4, canStart: false},{id: 54, row: 2,  col: 5, canStart: false},{id: 67, row: 2,  col: 6, canStart: false},{id: 80, row: 2,  col: 7, canStart: false},
			 		{id: 3,  row: 3,  col: 1, canStart: false},{id: 16, row: 3,  col: 2, canStart: false},{id: 29, row: 3,  col: 3, canStart: false},{id: 42, row: 3,  col: 4, canStart: false},{id: 55, row: 3,  col: 5, canStart: false},{id: 68, row: 3,  col: 6, canStart: false},{id: 81, row: 3,  col: 7, canStart: false},
			 		{id: 4,  row: 4,  col: 1, canStart: false},{id: 17, row: 4,  col: 2, canStart: false},{id: 30, row: 4,  col: 3, canStart: false},{id: 43, row: 4,  col: 4, canStart: false},{id: 56, row: 4,  col: 5, canStart: false},{id: 69, row: 4,  col: 6, canStart: false},{id: 82, row: 4,  col: 7, canStart: false},
			 		{id: 5,  row: 5,  col: 1, canStart: false},{id: 18, row: 5,  col: 2, canStart: false},{id: 31, row: 5,  col: 3, canStart: false},{id: 44, row: 5,  col: 4, canStart: false},{id: 57, row: 5,  col: 5, canStart: false},{id: 70, row: 5,  col: 6, canStart: false},{id: 83, row: 5,  col: 7, canStart: false},
			 		{id: 6,  row: 6,  col: 1, canStart: false},{id: 19, row: 6,  col: 2, canStart: false},{id: 32, row: 6,  col: 3, canStart: false},{id: 45, row: 6,  col: 4, canStart: false},{id: 58, row: 6,  col: 5, canStart: false},{id: 71, row: 6,  col: 6, canStart: false},{id: 84, row: 6,  col: 7, canStart: false},
			 		{id: 7,  row: 7,  col: 1, canStart: false},{id: 20, row: 7,  col: 2, canStart: false},{id: 33, row: 7,  col: 3, canStart: false},{id: 46, row: 7,  col: 4, canStart: false},{id: 59, row: 7,  col: 5, canStart: false},{id: 72, row: 7,  col: 6, canStart: false},{id: 85, row: 7,  col: 7, canStart: false},
			 		{id: 8,  row: 8,  col: 1, canStart: false},{id: 21, row: 8,  col: 2, canStart: false},{id: 34, row: 8,  col: 3, canStart: false},{id: 47, row: 8,  col: 4, canStart: false},{id: 60, row: 8,  col: 5, canStart: false},{id: 73, row: 8,  col: 6, canStart: false},{id: 86, row: 8,  col: 7, canStart: false},
			 		{id: 9,  row: 9,  col: 1, canStart: false},{id: 22, row: 9,  col: 2, canStart: false},{id: 35, row: 9,  col: 3, canStart: false},{id: 48, row: 9,  col: 4, canStart: false},{id: 61, row: 9,  col: 5, canStart: false},{id: 74, row: 9,  col: 6, canStart: false},{id: 87, row: 9,  col: 7, canStart: false},
			 		{id: 10, row: 10, col: 1, canStart: false},{id: 23, row: 10, col: 2, canStart: false},{id: 36, row: 10, col: 3, canStart: false},{id: 49, row: 10, col: 4, canStart: false},{id: 62, row: 10, col: 5, canStart: false},{id: 75, row: 10, col: 6, canStart: false},{id: 88, row: 10, col: 7, canStart: false},
			 		{id: 11, row: 11, col: 1, canStart: false},{id: 24, row: 11, col: 2, canStart: false},{id: 37, row: 11, col: 3, canStart: false},{id: 50, row: 11, col: 4, canStart: false},{id: 63, row: 11, col: 5, canStart: false},{id: 76, row: 11, col: 6, canStart: false},{id: 89, row: 11, col: 7, canStart: false},
			 		{id: 12, row: 12, col: 1, canStart: false},{id: 25, row: 12, col: 2, canStart: false},{id: 38, row: 12, col: 3, canStart: false},{id: 51, row: 12, col: 4, canStart: false},{id: 64, row: 12, col: 5, canStart: false},{id: 77, row: 12, col: 6, canStart: false},{id: 90, row: 12, col: 7, canStart: false},
			 		{id: 13, row: 13, col: 1, canStart: false},{id: 26, row: 13, col: 2, canStart: false},{id: 39, row: 13, col: 3, canStart: false},{id: 52, row: 13, col: 4, canStart: false},{id: 65, row: 13, col: 5, canStart: false},{id: 78, row: 13, col: 6, canStart: false},{id: 91, row: 13, col: 7, canStart: false}
				]
			}
		],

		periodList: [1,2,3,4,5,6,7,8,9,10,11,12,13],
		dayList: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
	},
	methods: {
		
	}
})